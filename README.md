# client

#### 介绍

一款基于CEF内核的浏览器，增加了全屏锁定、右键屏蔽等安全功能，一般来说，凡是能用Chrome进行的在线考试，均可使用本软件来增强考试的安全性。
为支持管理机远程启动本软件，在产品发布时，考场监听器与本软件打包在一起。


#### 开发工具
采用 Pascal 语言开发，推荐使用 Lazarus，可兼容 Delphi 7.0 及以上版本


#### 安装教程

绿色免安装，解压即用


更多内容参见： [https://gitee.com/examclient/manager](https://gitee.com/examclient/manager)


2021.10.07